import React, { Component } from 'react'
import todosList from './todos.json'
import { v4 as uuidv4 } from 'uuid'
import { render } from 'react-dom'

export default class App extends Component {
  state = {
    todos: todosList,
    value: ''
  }

  ACTION = {
    ADD_TODO: 'add-todo',
    TOGGLE_TODO: 'toggle-todo',
    DELETE_TODO: 'delete todo'
  }

  handleText = event => {
    this.setState({ value: event.target.value })
  }

  handleSubmit = event => {
    if (event.key === 'Enter') {
      this.handleAddTodo()
    }
  }

  handleAddTodo = () => {
    console.log('Todo being handled')
    const newTodo = {
      userId: 1,
      id: uuidv4(),
      title: this.state.value,
      completed: false
    }
    const newTodos = [...this.state.todos, newTodo]
    console.log(newTodos)
    this.setState({
      todos: newTodos,
      value: ''
    })
  }

  handleCheck = checkId => {
    console.log('Checked')
    const newTodos = this.state.todos.map(todoItem => {
      if (todoItem.id === checkId) {
        todoItem.completed = !todoItem.completed
      }
      return todoItem
    })
    this.setState({ todos: newTodos })
  }
  handleDeleteCompleted = todoId => {
    const newTodos = this.state.todos.filter(
      todoItem => this.state.todoItem.id !== true
    )
    this.setState({ todos: newTodos })
  }
  handleDelete = todoId => {
    const newTodos = this.state.todos.filter(todoItem => todoItem.id !== todoId)
    this.setState({ todos: newTodos })
  }

  handleClearComplete = () => {
    const newArray = this.state.todos.filter(
      todoItem => todoItem.completed === false
    )
    this.setState({ todos: newArray })
  }

  newTodos = {
    userId: 1,
    id: 1,
    title: 'delete',
    completed: false
  }

  reducer = (todos, action) => {
    switch (action.type) {
      case this.ACTION.ADD_TODO:
        return [...todos, this.newTodo(action.payload.name)]

      case this.ACTION.TOGGLE_TODO:
        return todos.map(todo => {
          if (todo.id === action.payload.id) {
            return { ...todo, complete: !todo.complete }
          }
          return todo
        })

      case this.ACTION.DELETE_TODO:
        return todos.filter(todo => todo.id !== action.payload.id)
      default:
        return todos
    }
  }

  newTodo (name) {
    return {
      id: Date.now(),
      name: name,
      complete: false
    }
  }
  render () {
    return (
      <section className='todoapp'>
        <header className='header'>
          <h1>todos</h1>

          <input
            className='new-todo'
            placeholder='Add Todo Item'
            autoFocus
            onChange={this.handleText}
            onKeyDown={this.handleSubmit}
            value={this.state.value ? this.state.value : ''}
          />
        </header>
        <TodoList
          todos={this.state.todos}
          handleCheck={this.handleCheck}
          handleDelete={this.handleDelete}
        />

        <footer className='footer'>
          <span className='todo-count'>
            <strong>0</strong> item(s) left
          </span>
          <button
            onClick={this.handleClearComplete}
            className='clear-completed'
          >
            Clear completed
          </button>
        </footer>
      </section>
    )
  }
}

class TodoList extends Component {
  render () {
    return (
      <section className='main'>
        <ul className='todo-list'>
          {this.props.todos.map(todo => (
            <TodoItem
              title={todo.title}
              completed={todo.completed}
              id={todo.id}
              handleDelete={this.props.handleDelete}
              handleCheck={this.props.handleCheck}
              handleToggle={this.props.handleToggle}
            />
          ))}
        </ul>
      </section>
    )
  }
}

class TodoItem extends Component {
  render () {
    return (
      <li className={this.props.completed ? 'completed' : ''}>
        <div className='view'>
          <input
            onChange={event => this.props.handleCheck(this.props.id)}
            className='toggle'
            type='checkbox'
            checked={this.props.completed}
          />

          <label>{this.props.title}</label>
          <button
            className='destroy'
            onClick={event => this.props.handleDelete(this.props.id)}
          >
            {' '}
          </button>
        </div>
      </li>
    )
  }
}
